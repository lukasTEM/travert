		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark" style="background: url('<?php echo get_template_directory_uri() . '/uploads/footer-bg.jpg' ?>') repeat;background-size: cover;">

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						<img src="<?php echo get_template_directory_uri() . '/uploads/travert_logo_final_white_sirka500pix.png' ?>" alt="" class="footer-logo">
					</div>

					<div class="col_half col_last tright">
						<div class="copyrights-menu copyright-links fright clearfix">
							<?php
          wp_nav_menu( array( 
          'theme_location' => 'extra-menu' 
           ) ); 
          ?><br>
							Copyrights &copy; 2018 All Rights Reserved by Travert s.r.o.<br>
							Created by <a href="https://www.studiotem.com" class="createdby nomargin" target="_blank">Studio TEM</a>
						</div>
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end-->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- WP Footer
	============================================= -->
	<?php wp_footer() ?>

</body>
</html>