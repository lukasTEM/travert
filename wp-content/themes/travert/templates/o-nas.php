<?php
/* Template Name: O nás */

get_header(); ?>
<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-pattern">

			<div class="container clearfix">
      <?php
		  $hlava_stranky = get_field('hlava_stranky');	
      if( $hlava_stranky ): ?>
				<h1><?php echo $hlava_stranky['nadpis']; ?></h1>
				<span><?php echo $hlava_stranky['podnadpis']; ?></span><?php endif; ?>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">
      
			<div class="content-wrap">
        <?php
		  $obrazok_v_pravo = get_field('obrazok_v_pravo');	
      if( $obrazok_v_pravo): ?>
				<div class="container clearfix">
        
				<div class="col_three_fifth">
        	<div class="heading-block">
        		<h4><?php echo $obrazok_v_pravo['nadpis']; ?></h4>
        	</div>
          <?php echo $obrazok_v_pravo['obsah']; ?>
          </div>
          <div class="col_two_fifth topmargin col_last" style="min-height: 350px">
	         <a href="<?php echo $obrazok_v_pravo['obrazok-1']['url']; ?>" data-lightbox="image">
		        <img src="<?php echo $obrazok_v_pravo['obrazok-1']['url']; ?>" alt="<?php echo $orazok_v_pravo['obrazok-1']['alt']; ?>">
            	</a>
          </div>
					<div class="line"></div>
         
          </div>
       <?php endif; ?>
        <?php
		         $obrazok_v_lavo = get_field('obrazok_v_lavo');	
            if( $obrazok_v_lavo): ?>
       	<div class="container clearfix">
         <div class="col_two_fifth topmargin nobottommargin" style="min-height: 350px;">
	        <a href="<?php echo $obrazok_v_lavo['obrazok-2']['url']; ?>" data-lightbox="image">
		        <img src="<?php echo $obrazok_v_lavo['obrazok-2']['url']; ?>" alt="<?php echo $obrazok_v_lavo['obrazok-2']['alt']; ?>">
            	</a>
          </div>
			 <div class="col_three_fifth nobottommargin col_last">
        

        	<div class="heading-block">
        		<h4><?php echo $obrazok_v_lavo['nadpis']; ?></h4>
        	</div>
         <?php echo $obrazok_v_lavo['obsah']; ?>
          </div>
         
					<div class="line"></div>
       
          </div>
     
			</div>
      <?php endif; ?> 
    
  <?php if( have_rows('pocitadlo') ): ?>
    <div class="section nomargin">
	<div class="container clearfix">
	 

	<?php while( have_rows('pocitadlo') ): the_row(); 
   
		// vars
    $ikona = get_sub_field('ikona');
		$cislo = get_sub_field('cislo');
		$popis = get_sub_field('popis');

		?>
     <div class="col_one_fourth nobottommargin center col_last" data-animate="bounceIn"> 
	     <?php echo $ikona; ?>
			<div class="counter counter-lined"><span data-from="100" data-to="<?php echo $cislo; ?>" data-refresh-interval="50" data-speed="2000"></span>k+</div>
			<h5><?php echo $popis; ?></h5>
      </div>
	<?php endwhile; ?>
      </div>
<?php endif; ?>
   
</div>
       <div class="content-wrap">
        <?php
		  $obrazok_v_pravo_1 = get_field('obrazok_v_pravo_1');	
      if( $obrazok_v_pravo_1): ?>
				<div class="container clearfix">
        
				<div class="col_three_fifth">
        	<div class="heading-block">
        		<h4><?php echo $obrazok_v_pravo_1['nadpis']; ?></h4>
        	</div>
          <?php echo $obrazok_v_pravo_1['obsah']; ?>
          </div>
          <div class="col_two_fifth topmargin col_last" style="min-height: 350px">
	         <a href="<?php echo $obrazok_v_pravo_1['obrazok-3']['url']; ?>" data-lightbox="image">
		        <img src="<?php echo $obrazok_v_pravo_1['obrazok-3']['url']; ?>" alt="<?php echo $obrazok_v_pravo_1['obrazok-3']['alt']; ?>">
            	</a>
          </div>
					<div class="line"></div>
         
          </div>
       <?php endif; ?>
       <?php
		         $obrazok_v_lavo_1 = get_field('obrazok_v_lavo_1');	
            if( $obrazok_v_lavo_1): ?>
       	<div class="container clearfix">
         <div class="col_two_fifth topmargin nobottommargin" style="min-height: 350px;">
	        <a href="<?php echo $obrazok_v_lavo_1['obrazok-4']['url']; ?>" data-lightbox="image">
		        <img src="<?php echo $obrazok_v_lavo_1['obrazok-4']['url']; ?>" alt="<?php echo $obrazok_v_lavo_1['obrazok-4']['alt']; ?>">
            	</a>
          </div>
			 <div class="col_three_fifth nobottommargin col_last">
        

        	<div class="heading-block">
        		<h4><?php echo $obrazok_v_lavo_1['nadpis']; ?></h4>
        	</div>
         <?php echo $obrazok_v_lavo_1['obsah']; ?>
          </div>
         
					<div class="line"></div>
       
          </div>  
          <?php endif; ?> 
        <?php
		  $obrazok_v_pravo_2 = get_field('obrazok_v_pravo_2');	
      if( $obrazok_v_pravo_2): ?>
				<div class="container clearfix">
        
				<div class="col_three_fifth">
        	<div class="heading-block">
        		<h4><?php echo $obrazok_v_pravo_2['nadpis']; ?></h4>
        	</div>
          <?php echo $obrazok_v_pravo_2['obsah']; ?>
          </div>
          <div class="col_two_fifth topmargin col_last" style="min-height: 350px">
	         <a href="<?php echo $obrazok_v_pravo_2['obrazok-5']['url']; ?>" data-lightbox="image">
		        <img src="<?php echo $obrazok_v_pravo_2['obrazok-5']['url']; ?>" alt="<?php echo $obrazok_v_pravo_2['obrazok-5']['alt']; ?>">
            	</a>
          </div>
					<div class="line"></div>
         
          </div>
                         
     
			</div>
      <?php endif; ?>    
          
           

			<div class="content-wrap">

				

				<!-- CTA
				============================================= -->
				<a href="<?php the_field('odkaz_pre_button'); ?>" class="button button-full center tright footer-stick">
					<div class="container clearfix">
						<?php the_field('cta'); ?> <i class="icon-caret-right" style="top:4px;"></i>
					</div>
				</a>

			</div>

		</section><!-- #content end -->
  <?php get_footer(); ?>