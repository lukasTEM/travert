<?php
/* Template Name: Realizácie */

get_header(); ?>
  	<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-dark">
      <?php
		  $hlavicka_stranky = get_field('hlavicka_stranky');
      if( $hlavicka_stranky ): ?>
			<div class="container clearfix">
				<h1><?php echo $hlavicka_stranky['nadpis']; ?></h1>
				<span></span>
			</div>
      <?php endif; ?>
		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<!-- Text
			============================================= -->
			<div class="content-wrap nobottompadding toppadding-sm">
				<div class="container clearfix">

					<div class="col_first text_field">
						<?php the_field('obsah'); ?>
					</div>



					<div class="clear"></div>

				</div>
			</div>

			<!-- Portfolio Archive
			============================================= -->
			<div class="content-wrap toppadding-sm">

				<div class="container clearfix">

					<!-- Portfolio Filter
					============================================= -->
					<div class="button-group filter-button-group">
						<button class="button-filter" data-filter="*">Všetky</a></li>
            <?php
              $categories = get_categories();
              $i = 0;
              foreach($categories as $category) {
                if(0 == $i){
                  echo '<button class="button-filter is-checked" data-filter="' . $category->slug . '">' . $category->name . '</button>';
                }else{
                  echo '<button class="button-filter" data-filter="' . $category->slug . '">' . $category->name . '</button>';
                }

                $i++;
              }
            ?>
					<!-- #portfolio-filter end -->
          	<div id="portfolio-shuffle" class="portfolio-shuffle" data-container="#portfolio">
						<i class="icon-random"></i>
					</div>
         </div>

					<div class="clear"></div>


					<!-- Portfolio Items
					============================================= -->

    		  <div id="portfolio" class="portfolio grid-container portfolio-4 clearfix">
          <?php
          $loop = new WP_Query(array('post_type' => 'referencia', 'posts_per_page' => 200));
          if ($loop->have_posts()):
            while($loop->have_posts()) {
              $loop->the_post();
               $categories = get_the_category();
                $cls = '';
                  if ( ! empty( $categories ) ) {
                    foreach ( $categories as $cat ) {
                    $cls .= $cat->slug . ' ';
                   }
                  }
              ?>

              <article class="portfolio-item <?php echo $cls; ?> pf-icons">
    							<div class="portfolio-image">
                    <a href="<?php echo get_permalink(); ?>">
                    <?php
                     $images = get_field('galeria_projektu');
                     $image_1 = $images[0];
                    ?>
                    <img src="<?php echo $image_1['sizes']['custom-project-thumb']; ?>">
                    </a>
                    <div class="portfolio-overlay">
    									<a href="<?php echo get_permalink(); ?>" class="center-icon"><i class="icon-book2"></i></a>
    								</div>
    							</div>
    							<div class="portfolio-desc">
    								<h3><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
    								<span><?php $categories = get_the_category();
                    $separator = ' ';
                    $output = '';
                    if ( ! empty( $categories ) ) {
                        foreach( $categories as $category ) {
                            $output .= '<a>' . esc_html( $category->name ) . '</a>' . $separator;
                        }
                        echo trim( $output, $separator );
                    } ?> </span>
    							</div>
    						</article>
               <?php
            }
          endif;
          wp_reset_query();
          ?>

					</div><!-- #portfolio end -->

				</div>

			</div>

		</section><!-- #content end -->
      <?php get_footer(); ?>