<?php
/* Template Name: Výhody travertínu v2*/

get_header(); ?>
<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-dark">

			<div class="container clearfix">
        <h1><?php echo get_the_title(); ?></h1>
				<span><?php the_field('podnadpis_stranky'); ?></span>
			</div>

		</section><!-- #page-title end -->
    <!-- Content
		============================================= -->
		<section id="content">
    <div class="content-wrap">
  
    <?php
    if( have_rows('obsah') ):
    while ( have_rows('obsah') ) : the_row();
    if( get_row_layout() == 'obrazok_lavo' ):?>
    <div class="container clearfix">
    <div class="col_two_fifth topmargin nobottommargin" style="min-height: 350px; padding-bottom: 90px;">
     <?php
       $imageArray = get_sub_field('obrazok'); // Array returned by Advanced Custom Fields
       $imageAlt = esc_attr($imageArray['alt']); // Grab, from the array, the 'alt'
       $imageURL = esc_url($imageArray['url']); // Grab the full size version 
       $imageThumbURL = esc_url($imageArray['sizes']['vyhody']); //grab from the array, the 'sizes', and from it, the 'thumbnail'
       ?>
      <a href="<?php echo $imageURL; ?>" data-lightbox="image">
        <img src="<?php echo $imageThumbURL;?>" alt="<?php echo $imageAlt; ?>">
      </a> 
    </div>
    <div class="col_three_fifth nobottommargin col_last">
    <div class="heading-block">
    <h4><?php the_sub_field('nadpis'); ?></h4>
    </div>
    <p><?php the_sub_field('obsah'); ?></p>
    </div>
  
    <div class="line"></div>
    </div>
     <?  
    
    else :
    endif;
    ?>
     
     
    <?php
    
    if( get_row_layout() == 'obrazok_pravo' ):?>
    <div class="container clearfix">
    <div class="col_three_fifth">
    <div class="heading-block">
		<h4><?php the_sub_field('nadpis'); ?></h4>
	</div>
  <p><?php the_sub_field('obsah'); ?></p>
</div>

<div class="col_two_fifth topmargin col_last" style="min-height: 350px;">
	 <?php
       $imageArray = get_sub_field('obrazok'); // Array returned by Advanced Custom Fields
       $imageAlt = esc_attr($imageArray['alt']); // Grab, from the array, the 'alt'
       $imageURL = esc_url($imageArray['url']); // Grab the full size version 
       $imageThumbURL = esc_url($imageArray['sizes']['vyhody']); //grab from the array, the 'sizes', and from it, the 'thumbnail'
       ?>
      <a href="<?php echo $imageURL; ?>" data-lightbox="image">
        <img src="<?php echo $imageThumbURL;?>" alt="<?php echo $imageAlt; ?>">
      </a> 
</div>

<div class="line"></div>
</div>
     <?  
    
    else :
    endif;
    ?>
    <?php
    if( get_row_layout() == 'textove_pole' ):?>
     <div class="container clearfix">
<div class="col_first text_field"><?php the_sub_field('obsah'); ?></div> 
<div class="line"></div></div>
     <?  
    endif;
    endwhile;
    else :
    endif;
    ?>
     
   
   </div> 
     
    <div class="content-wrap">
    	<!-- CTA
				============================================= -->
				<a href="<?php the_field('odkaz_pre_button'); ?>" class="button button-full center tright footer-stick">
					<div class="container clearfix">
						<?php the_field('cta'); ?> <i class="icon-caret-right" style="top:4px;"></i>
					</div>
				</a>

			</div>

		</section><!-- #content end -->
  <?php get_footer(); ?>