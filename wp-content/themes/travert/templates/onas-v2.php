<?php
/* Template Name: O nás v2*/

get_header(); ?>
<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-dark">

			<div class="container clearfix">
        <h1><?php echo get_the_title(); ?></h1>
				<span><?php the_field('podnadpis_stranky'); ?></span>
			</div>

		</section><!-- #page-title end -->
    <!-- Content
		============================================= -->
		<section id="content">
    <div class="content-wrap">
    
    <?php
    if( have_rows('obsah') ):
    while ( have_rows('obsah') ) : the_row();  
    if( get_row_layout() == 'obrazok_lavo' ):?>
    <div class="container clearfix">
    <div class="col_two_fifth topmargin nobottommargin" style="min-height: 350px;">
     <?php
       $imageArray = get_sub_field('obrazok'); // Array returned by Advanced Custom Fields
       $imageAlt = esc_attr($imageArray['alt']); // Grab, from the array, the 'alt'
       $imageURL = esc_url($imageArray['url']); // Grab the full size version 
       $imageThumbURL = esc_url($imageArray['sizes']['onas']); //grab from the array, the 'sizes', and from it, the 'thumbnail'
       ?>
      <a href="<?php echo $imageURL; ?>" data-lightbox="image">
        <img src="<?php echo $imageThumbURL;?>" alt="<?php echo $imageAlt; ?>">
      </a>
    </div>
    <div class="col_three_fifth nobottommargin col_last">
    <div class="heading-block">
    <h4><?php the_sub_field('nadpis'); ?></h4>
    </div>
    <p><?php the_sub_field('obsah'); ?></p>
    </div>
    
    <div class="line"></div>
    </div>
     <?  
    
    else :
    endif;
    ?>
      
  <?php if( have_rows('pocitadlo') ): ?>
    <div class="section numbers_wrap">
	    <div class="container clearfix">
	 
  
	<?php while( have_rows('pocitadlo') ): the_row(); 
   
		// vars
    $ikona = get_sub_field('ikona');
		$cislo = get_sub_field('cislo');
		$popis = get_sub_field('popis');

		?>
     
     <div class="col_one_third nobottommargin center col_last" data-animate="bounceIn">
     <i class="i-plain i-xlarge divcenter <?php echo $ikona; ?>"></i>
     <div class="counter counter-lined"><span data-from="100" data-to="<?php echo $cislo; ?>" data-refresh-interval="50" data-speed="2000"></span>+</div> 
			<h5><?php echo $popis; ?></h5>
      </div>
	<?php endwhile; ?>
   
      </div>
      </div>
       <?php endif; ?>  
     


     
    <?php
    
    if( get_row_layout() == 'obrazok_pravo' ):?>
    <div class="container clearfix">
    <div class="col_three_fifth">
    <div class="heading-block">
		<h4><?php the_sub_field('nadpis'); ?></h4>
	</div>
  <p><?php the_sub_field('obsah'); ?></p>
</div>

    <div class="col_two_fifth topmargin col_last" style="min-height: 350px">
      <?php
       $imageArray = get_sub_field('obrazok'); // Array returned by Advanced Custom Fields
       $imageAlt = esc_attr($imageArray['alt']); // Grab, from the array, the 'alt'
       $imageURL = esc_url($imageArray['url']); // Grab the full size version 
       $imageThumbURL = esc_url($imageArray['sizes']['onas']); //grab from the array, the 'sizes', and from it, the 'thumbnail'
       ?>
      <a href="<?php echo $imageURL; ?>" data-lightbox="image">
        <img src="<?php echo $imageThumbURL;?>" alt="<?php echo $imageAlt; ?>">
      </a>
    </div>

<div class="line"></div>
</div>

     <?  
    endif;
    endwhile;
    else :
    endif;
    ?>
   </div>    
   

     
    <div class="content-wrap">
    	<!-- CTA
				============================================= -->
				<a href="<?php the_field('odkaz_pre_button'); ?>" class="button button-full center tright footer-stick">
					<div class="container clearfix">
						<?php the_field('cta'); ?> <i class="icon-caret-right" style="top:4px;"></i>
					</div>
				</a>

			</div>

		</section><!-- #content end -->
  <?php get_footer(); ?>