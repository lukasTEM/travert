<?php
/* Template Name: Kontakt */

get_header(); ?>


		<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-dark">
     <?php
		  // vars
       $hlava_stranky = get_field('hlava_stranky');	
       if( $hlava_stranky ): ?>
			<div class="container clearfix">
				<h1><?php echo $hlava_stranky['nadpis']; ?></h1>
				<div class="contact-title">
					<div class="col_half nobottommargin">
						<abbr title="Telefónne číslo">Telefón:</abbr> <?php echo $hlava_stranky['telefon']; ?>
					</div>
					<div class="col_half col_last nobottommargin">
						<abbr title="Emailová Adresa">Email:</abbr> <a href="mailto:<?php echo $hlava_stranky['email']; ?>"><?php echo $hlava_stranky['email']; ?></a>
					</div>
				</div>
			</div>
     <?php endif; ?>
		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<!-- Postcontent
					============================================= -->
					<div class="postcontent nobottommargin">

						<h3>Napíšte nám správu alebo nám zavolajte</h3>

						<!-- <div class="contact-widget"> -->

							<!-- <div class="contact-form-result"></div> -->

              <?php echo do_shortcode( '[formidable id=5]' ); ?>
						
						<!-- </div> -->

					</div><!-- .postcontent end -->

					<!-- Sidebar
					============================================= -->
					<div class="sidebar col_last nobottommargin contact-sidebar">
           <?php
		        // vars
            $kontaktna_osoba = get_field('kontaktna_osoba');	
            if( $kontaktna_osoba ): ?>
						<img src="<?php echo $kontaktna_osoba['fotka']['sizes']['portret']; ?>" alt="Vladimír Jakáb">
						
						<p class="contact-person">
						<?php echo $kontaktna_osoba['pozicia']; ?><br>
							<strong><?php echo $kontaktna_osoba['meno']; ?></strong>
						</p>

						<p class="contact-person">
							<abbr title="Telefónne číslo">Telefón:</abbr>  <?php echo $kontaktna_osoba['telefon']; ?><br>
							<abbr title="Emailová Adresa">Email:</abbr>  <a href="mailto:<?php echo $kontaktna_osoba['e-mail']; ?>"><?php echo $kontaktna_osoba['e-mail']; ?></a>
						</p>
          <?php endif; ?>
					</div><!-- .sidebar end -->

					<!-- Text
					============================================= -->

				</div>

			</div>

			<div class="content-wrap notoppadding nobottompadding contact-info">

				<div class="container clearfix">

					<!-- Columns
					============================================= -->
					<div class="col_half text-column">
          <?php
		        // vars
            $kontaktne_a_fakturacne_udaje = get_field('kontaktne_a_fakturacne_udaje');	
            if( $kontaktne_a_fakturacne_udaje ): ?>
						<h4><?php echo $kontaktne_a_fakturacne_udaje['nadpis']; ?></h4>
					   <?php echo $kontaktne_a_fakturacne_udaje['udaje']; ?>
            <?php endif; ?>
					</div>

					<div class="col_half col_last">
          <?php
		        // vars
            $showroom = get_field('showroom');	
            if( $showroom ): ?>
						<h4><?php echo $showroom['nadpis']; ?></h4>

						<a href="<?php echo $showroom['obrazok_showroomu']['url']; ?>" data-lightbox="image">
							<img src="<?php echo $showroom['obrazok_showroomu']['sizes']['showroom']; ?>" alt="Lightbox Image">
						</a>
           <?php endif; ?>
					</div>

					<div class="clear"></div>
					<!-- Columns end -->

				</div>

			</div>

			<!-- Google Map
			============================================= -->
			<section id="google-map" class="gmap">
				<?php the_field('mapa'); ?>
			</section>

		</section><!-- #content end -->
<?php get_footer(); ?>
