<?php
/* Template Name: Homepage */

get_header(); ?>
	<!-- Headline/Slider
		============================================= -->
		<?php // sprav im prosim ta niekde moznost vybrat farbu pisma v slideri a opacity ciernej pre filter na fotku v slideri ?>
    <?php
		// vars
    $slider = get_field('slider');	
    if( $slider ): ?>
    		<section id="slider" class="slider-element slider-parallax" style="background: url('<?php echo $slider['obrazok_slideru']['sizes']['hp-img-slider']; ?>') no-repeat; background-size: cover; background-position: center;" data-height-xl="600" data-height-lg="500" data-height-md="400" data-height-sm="300" data-height-xs="250">
			<div class="slider-parallax-inner" style="background-color: rgba(0,0,0,<?php echo $slider['podfarbenie_slidera'] ?>);">

				<div class="container clearfix">
					<div class="vertical-middle dark center">

						<div class="heading-block nobottommargin center">
							<h1>
								<span class="text-rotater nocolor" data-separator="|" data-rotate="flipInX" data-speed="3500">
									<?php echo $slider['nadpis']; ?>
									<span class="t-rotate">
										<?php
											$i = 1;
											$length = count( $slider['nadpis_ktory_rotuje'] );
											foreach ( $slider['nadpis_ktory_rotuje'] as $nadpis ) { 
												echo $nadpis['element'];
												if ( $i < $length ) {
													echo '|';
												}
												$i++;
											} 
										?>
									</span>
								</span>
							</h1>
							<span class="d-none d-md-block">	<?php echo $slider['podnadpis']; ?></span>
						</div>

						<a href="#" data-scrollto="#content" class="button button-border button-light button-rounded button-reveal tright button-large topmargin d-none d-md-inline-block"><i class="icon-angle-down"></i><span><?php echo $slider['tlacidlo_']; ?></span></a>

					</div>
				</div>
			</div>
		</section>
   <?php endif; ?>
		<!-- Content
		============================================= -->
		<section id="content">

			<!-- Our Mission
			============================================= -->
      
			<div class="content-wrap nobottompadding">

				<div class="container clearfix">
           <?php
    		  // vars
          $boxy_pod_sliderom = get_field('boxy_pod_sliderom');	
          if( $boxy_pod_sliderom): ?>
					<div class="col_one_third">

						<div class="heading-block fancy-title nobottomborder title-bottom-border">
							<h4><?php echo $boxy_pod_sliderom['box-1-nadpis']; ?> <span><?php echo $boxy_pod_sliderom['box-1-nadpis-farebny-koniec']; ?></span>.</h4>
						</div>

						<p><?php echo $boxy_pod_sliderom['box-1-content']; ?></p>

					</div>

					<div class="col_one_third">

					  <div class="heading-block fancy-title nobottomborder title-bottom-border">
							<h4><?php echo $boxy_pod_sliderom['box-2-nadpis']; ?> <span><?php echo $boxy_pod_sliderom['box-2-nadpis-farebny-koniec']; ?></span>.</h4>
						</div>

						<p><?php echo $boxy_pod_sliderom['box-2-content']; ?></p>

					</div>

					<div class="col_one_third col_last">

					  <div class="heading-block fancy-title nobottomborder title-bottom-border">
							<h4><?php echo $boxy_pod_sliderom['box-3-nadpis']; ?> <span><?php echo $boxy_pod_sliderom['box-3-nadpis-farebny-koniec']; ?></span>.</h4>
						</div>

						<p><?php echo $boxy_pod_sliderom['box-3-content']; ?></p>

					</div>
             <?php endif; ?>
				</div>

				<!-- Headline
				============================================= -->
        
        <?php
    		  // vars
          $ponuka = get_field('ponuka');	
          if( $ponuka): ?>
				<section id="page-title" class="page-title-center page-title-dark bottommargin">
					<div class="container clearfix">
						<h1><?php echo $ponuka['nadpis']; ?></h1>
						<span><?php echo $ponuka['podnadpis']; ?></span>
					</div>
          <?php endif; ?>
				</section>
        
       
        <?php $args = array(
            'post_type' => 'produkty',
            'posts_per_page' => 5
        );
        $produkty = new WP_Query($args);
        while ( $produkty->have_posts() ) : $produkty->the_post(); 
        ?>
            <?php if($produkty->current_post % 2) : ?>
            <div class="row common-height clearfix product_preview">
            
            <div class="col-md-7 col-padding" style="background-color: #F5F5F5;">
        		<div>
        			<div class="heading-block">
              
        				<span class="before-heading color"><?php the_field('hlavny_podnadpis'); ?></span>
        				<h3><?php echo get_the_title(); ?></h3>
        			</div>
        
        			<div class="row clearfix">
        
        				<div class="col-lg-12">
        					<?php the_field('obsah_na_hlavnej_stranke'); ?>
        				</div>
        
        			</div>
        
        			<a href="<?php echo get_permalink(); ?>" class="button button-small button-border button-rounded button-fill button-brown nomargin">
        				<span>Objavte viacej</span>
        			</a>
        
        		</div>
        	</div>
            <?php 
            $images = get_field('galeria'); 
            $image_1 = $images[0]; 
            ?>    
            <div class="col-md-5 col-padding" style="background: url('<?php echo $image_1['sizes']['hp-img']; ?>') center center no-repeat; background-size: cover;"></div> 

       </div>
       
       <?php else : ?>
         <div class="row common-height clearfix product_preview">
                 <?php 
                 $images = get_field('galeria'); 
                  $image_1 = $images[0]; 
                  ?>  
                <div class="col-md-5 col-padding" style="background: url('<?php echo $image_1['sizes']['hp-img']; ?>') center center no-repeat; background-size: cover;"></div>
                 <div class="col-md-7 col-padding">
		            <div>
			         <div class="heading-block">
				        <span class="before-heading color"><?php the_field('hlavny_podnadpis'); ?></span>
				          <h3><?php echo get_the_title(); ?></h3>
			           </div>
                <div class="row clearfix">
                  <div class="col-lg-12">
            					<?php the_field('obsah_na_hlavnej_stranke'); ?>
                      </div>
                  </div>
            	    <a href="<?php echo get_permalink(); ?>" class="button button-small button-border button-rounded button-fill button-brown nomargin">
            				<span>Objavte viacej</span>
            			</a>
            	</div>
            	</div>
              
               </div>
              
            
          <?php endif; ?>  
           
        <?php endwhile; ?>

        <?php wp_reset_query() ?>
        
             <?php $call_to_action = get_field('call_to_action');
          if( $call_to_action): ?>
				<div class="promo promo-light promo-full bottommargin topmargin notopborder">
					<div class="container clearfix">
						<h3><?php echo $call_to_action['horny_riadok']; ?></span></h3>
						<span><?php echo $call_to_action['podnadpis']; ?></span>
						<a href="<?php the_field('smerovanie_buttonu'); ?>" class="button button-dark button-xlarge button-rounded"><?php echo $call_to_action['text_button']; ?></a>
					</div>
				</div>
         <?php endif; ?> 
          
        		</div>
           
		</section><!-- #content end -->
    
<?php get_footer(); ?>    