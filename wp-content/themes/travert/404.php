<?php
/* Template Name: 404 */

get_header(); ?>
	<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-dark">

			<div class="container clearfix">
				<h1>Page Not Found</h1>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="col_half nobottommargin">
						<div class="error404 center">404</div>
					</div>

					<div class="col_half nobottommargin col_last">

						<div class="heading-block nobottomborder">
							<h4>Hups!<br>Bohuzial, stranka, ktoru chcete zobrazit neexistuje.</h4>
							<span>Skontrolujte, ci adresa v prehliadaci je zadana spravne. Alebo:</span>
							<a href="<?php echo get_home_url(); ?>" class="button button-3d button-xlarge button-rounded button-brown" style="margin-top: 24px;">Naspat na hlavnu stranku</a>
						</div>

					</div>

				</div>

			</div>

		</section><!-- #content end -->
<?php get_footer(); ?>