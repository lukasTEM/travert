<?php
/* Template Name: portfolio*/

get_header(); ?>	
<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-dark">

			<div class="container clearfix">
       
				<h1 class="max"><?php echo get_the_title(); ?></h1>
     
				<div id="portfolio-navigation">
         <?php next_post_link('%link', '<i class="icon-angle-left"></i>')?>  
				 <a href="/referencia/"><i class="icon-line-grid"></i></a>
         <?php previous_post_link('%link', '<i class="icon-angle-right"></i>')?>
					 
				</div>
			</div>
       
		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap toppadding-sm">

				<div class="container clearfix">

					<!-- Portfolio Single Gallery
					============================================= -->
					<div class="col_full portfolio-single-image">
						<div class="fslider" data-arrows="true" data-animation="slide">
							<div class="flexslider">
								<div class="slider-wrap">
                <?php 
                  $images = get_field('galeria_projektu');
                  if( $images ): ?>
                <?php foreach( $images as $image ): ?>
                <div class="slide"><a href="#" onclick="return false;"><img src="<?php echo $image['sizes']['slider'];?>" alt="<?php echo $image['alt']; ?>"></a></div>
               <?php endforeach; ?>
              	</div>
              <?php endif; ?>
							</div>
						</div>
					</div><!-- .portfolio-single-image end -->

					<!-- Portfolio Single Content
					============================================= -->
					<div class="col_two_third portfolio-single-content nobottommargin">

						<!-- Portfolio Single - Description
						============================================= -->
						<div class="fancy-title title-dotted-border">
            <?php
		        $obsah = get_field('obsah');	
            if( $obsah ): ?>
							<h2><?php echo $obsah['nadpis']; ?></h2>
						</div>
            <?php echo $obsah['iframe_pre_video_volitelne']; ?>
						<div class="col_half nobottommargin">
							<?php echo $obsah['stlpec_1']; ?>
						</div>

						<div class="col_half col_last nobottommargin">
							<?php echo $obsah['stlpec_2']; ?>
             <?php endif; ?>
							<!-- Post Single - Share
								============================================= -->
								 <?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
							
						</div>
						<!-- Portfolio Single - Description End -->

					</div><!-- .portfolio-single-content end -->

					<div class="col_one_third col_last nobottommargin">

						<!-- Portfolio Single - Meta
						============================================= -->
						<ul class="portfolio-meta bottommargin-sm center">
             <?php
		         $cta_ = get_field('cta_');	
              if( $cta_ ): ?>
							<h2>
								<?php echo ( isset( $cta_['nadpis'] ) && $cta_['nadpis'] ) ? $cta_['nadpis'] : 'Páči sa vám táto realizácia?' ?>
							</h2>
              <?php $cta = get_field( 'cta_' ) ?>

				            <?php if ( $cta && is_array( $cta ) ): ?>

				            	<?php $title = ( isset( $cta['tlacidlo']['title'] ) && $cta['tlacidlo']['title'] && $cta['tlacidlo']['title'] !== '' ) ? $cta['tlacidlo']['title'] : 'Ozvite sa nám' ?>

					            <a href="<?php echo ( isset( $cta['tlacidlo']['url'] ) && $cta['tlacidlo']['url'] ) ? $cta['tlacidlo']['url'] : get_permalink( 40 ) ?>" 
					            class="button button-3d button-large button-rounded" 
					            target="<?php echo ( isset( $cta['tlacidlo']['target'] ) && $cta['tlacidlo']['target'] ) ? $cta['tlacidlo']['target'] : '' ?>" 
					            title="<?php echo $title ?>"
					            >
					            	<?php echo $title ?>
					            </a>

					        <?php endif; ?>

					       

						</ul>
            <?php endif; ?>
						<!-- Portfolio Single - Meta End -->

					</div><!-- .portfolio-single-content end -->

					<div class="clear"></div>

					<div class="divider divider-center"><i class="icon-circle"></i></div>

					<!-- Related Portfolio Items
					============================================= -->
					<h4>Podobné projekty:</h4>

					<div id="related-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="30" data-nav="false" data-autoplay="5000" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-xl="4">
            <?php
            
          $loop = new WP_Query(array('post_type' => 'referencia', 'posts_per_page' => 12, 'category_name' => get_the_category()[0]->slug));
          if ($loop->have_posts()):
            while($loop->have_posts()) { 
              $loop->the_post();
              ?>
             <div class="oc-item">
							<div class="iportfolio">
								<div class="portfolio-image">
                    <a href="<?php echo get_permalink(); ?>">
                     <?php 
                     $images = get_field('galeria_projektu'); 
                     $image_1 = $images[0]; 
                    ?>    
                    <img src="<?php echo $image_1['sizes']['custom-project']; ?>">
                    </a>
                    <div class="portfolio-overlay">
    									<a href="<?php echo get_permalink(); ?>" class="center-icon"><i class="icon-book2"></i></a>
    								</div>
    							</div>
    							<div class="portfolio-desc">
    								<h3><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
    								<span><span><?php $categories = get_the_category();
                    $separator = ' ';
                    $output = '';
                    if ( ! empty( $categories ) ) {
                        foreach( $categories as $category ) {
                            $output .= '<a>' . esc_html( $category->name ) . '</a>' . $separator;
                        }
                        echo trim( $output, $separator );
                    } ?> </span></span>
    							</div>
    						 </div>
						</div>
               <?php
            }
          endif;
          wp_reset_query();
          ?>
				
			

					</div><!-- .portfolio-carousel end -->

				</div>

			</div>

		</section><!-- #content end -->
<?php get_footer(); ?>