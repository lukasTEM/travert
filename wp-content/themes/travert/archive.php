<?php
/* Template Name: Blog */

get_header(); ?>
<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-pattern">

			<div class="container clearfix">
       <?php
		    $hlavicka_stranky = get_field('hlavicka_stranky');	
        if( $hlavicka_stranky ): ?>
				<h1><?php echo $hlavicka_stranky['nadpis']; ?></h1>
				<span><?php echo $hlavicka_stranky['podnadpis']; ?></span>
			</div><?php endif; ?>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
         <div id="posts" class="small-thumbs">
         	 <?php
          $loop = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 8, 'orderby' => 'ID', 'order' => 'DESC'));
          if ($loop->have_posts()):
            while($loop->have_posts()) {
              $loop->the_post();
              ?>
              <div class="entry clearfix">
	               <div class="entry-image">
                 	<a href="<?php echo get_permalink(); ?>" data-lightbox="image"><img class="image_fade" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Blog - obrázok"></a>
                   </div>
                    <div class="entry-c">
                		<div class="entry-title">
                			<h2><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
                		</div>
                		<ul class="entry-meta clearfix">
                			<li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
                			<li><i class="icon-folder-open"></i><?php $categories = get_the_category();
                        if ( ! empty( $categories ) ) {
                        echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';}?></li>
                		</ul>
                		<div class="entry-content">
                			<p><?php the_excerpt(); ?></p>
                			<a href="<?php echo get_permalink(); ?>"class="more-link">Čítať viac</a>
                		</div>
                	</div>
                  </div>
                   
               <?php
            }
          endif;
          wp_reset_query();
          ?>

					

					</div><!-- #posts end -->
        	<!-- Pagination
					============================================= -->
					<div class="row mb-3">
						<div class="col-12">
							<a href="#" class="btn btn-outline-secondary float-left">&larr; Older</a>
							<a href="#" class="btn btn-outline-dark float-right">Newer &rarr;</a>
						</div>
					</div>
					<!-- .pager end -->

				</div>

			</div>

		</section><!-- #content end -->
  <?php get_footer(); ?>