<?php
/* Template Name: Blog */

get_header(); ?>
<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-dark">

			<div class="container clearfix">
       <?php
		    $hlavicka_stranky = get_field('hlavicka_stranky', get_option( 'page_for_posts' ));	
        if( $hlavicka_stranky ): ?>
				<h1><?php echo $hlavicka_stranky['nadpis']; ?></h1>
				<span><?php echo $hlavicka_stranky['podnadpis']; ?></span>
			</div><?php endif; ?>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
         <div id="posts" class="small-thumbs">
         	 <?php
           $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
          $loop = new WP_Query(array('post_type' => 'post', 'post_status'=>'publish', 'posts_per_page' => get_option( 'posts_per_page' ), 'orderby' => 'ID', 'order' => 'DESC', 'paged' => $paged));
          if ($loop->have_posts()):
            while($loop->have_posts()) {
              $loop->the_post();
              ?>
              <div class="entry clearfix">
	               <div class="entry-image">
                  <?php 
                  $hlavna_fotka = get_field('hlavna_fotka');
                  if( !empty($hlavna_fotka) ): ?>
                 	<a href="<?php echo get_permalink( $post->ID); ?>"><img class="image_fade" src="<?php echo $hlavna_fotka['sizes']['blog-thumbs']; ?>"></a><?php endif; ?>
                   </div>
                    <div class="entry-c">
                		<div class="entry-title">
                			<h2><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
                		</div>
                		<ul class="entry-meta clearfix">
                			<li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
                			<li><i class="icon-folder-open"></i><span><?php $categories = get_the_category();
                    $separator = ' ';
                    $output = '';
                    if ( ! empty( $categories ) ) {
                        foreach( $categories as $category ) {
                            $output .= esc_html( $category->name ) . $separator;
                        }
                        echo trim( $output, $separator );
                    } ?> </span></li>
                		</ul>
                		<div class="entry-content">
                			<p><?php echo custom_field_excerpt(); ?></p>
                			<a href="<?php echo get_permalink(); ?>"class="more-link">Čítať viac</a>
                		</div>
                	</div>
                  </div>
                  
                   
               <?php
            }
          endif;
          wp_reset_query();
          ?>
           <?php 

                           if (function_exists('custom_pagination')) :
                               custom_pagination($loop->max_num_pages,"",$paged);
                           endif;

                       ?>
					

					</div><!-- #posts end -->
        	<!-- Pagination
					============================================= -->
					<div class="row mb-3">
						<div class="col-12">
							<a href="#" class="btn btn-outline-secondary float-left">&larr; Older</a>
							<a href="#" class="btn btn-outline-dark float-right">Newer &rarr;</a>
						</div>
					</div>
					<!-- .pager end -->

				</div>

			</div>

		</section><!-- #content end -->
  <?php get_footer(); ?>