<?php 
function register_my_menus() {
	register_nav_menus(
		array(
			'header-menu' => __( 'Header Menu' ),
			'extra-menu' => __( 'Extra Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );
add_theme_support( 'post-thumbnails' ); 
add_action('admin_init', 'remove_textarea');

		function remove_textarea() {
						remove_post_type_support( 'page', 'editor' );
		}


/************************************************************************************/
/* Remove admin bar */
/************************************************************************************/
add_filter( 'show_admin_bar', '__return_false' );

function custom_short_excerpt($excerpt){
	return substr($excerpt, 0, 100);
}
add_filter('the_excerpt', 'custom_short_excerpt');


/************************************************************************************/
/* Launch script after formidable is sent */
/************************************************************************************/
function formidable_sent( $entry_id, $form_id ) {
    ?>
        <script>
            window.dataLayer = window.dataLayer || []
              window.dataLayer.push({
               'event': 'formSubmission',
            });
        </script>
    <?php
}
add_action('frm_after_create_entry', 'formidable_sent', 2, 2);


/************************************************************************************/
/* Enqueue scripts and styles */
/************************************************************************************/
function futurenow_scripts() {

	wp_enqueue_style( 'font', 'https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i', array(), time() );
	wp_enqueue_style( 'travert-bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), time() );
	wp_enqueue_style( 'travert-style', get_template_directory_uri() . '/style.css', array(), time() );
	wp_enqueue_style( 'travert-swiper', get_template_directory_uri() . '/css/swiper.css', array(), time() );
	wp_enqueue_style( 'travert-dark', get_template_directory_uri() . '/css/dark.css', array(), time() );
	wp_enqueue_style( 'travert-font-icons', get_template_directory_uri() . '/css/font-icons.css', array(), time() );
	wp_enqueue_style( 'travert-animate', get_template_directory_uri() . '/css/animate.css', array(), time() );
	wp_enqueue_style( 'travert-magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css', array(), time() );
	wp_enqueue_style( 'travert-responsive', get_template_directory_uri() . '/css/responsive.css', array(), time() );
	wp_enqueue_style( 'travert-colors', get_template_directory_uri() . '/css/colors.css', array(), time() );
	wp_enqueue_style( 'travert-custom', get_template_directory_uri() . '/css/custom.css', array(), time() );

	wp_enqueue_script( 'plugins-js', get_template_directory_uri() . '/js/plugins.js', array( 'jquery' ), time(), true );
	wp_enqueue_script( 'functions', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), time(), true );

}
add_action( 'wp_enqueue_scripts', 'futurenow_scripts' );


/************************************************************************************/
/* Admin menu custom order */
/************************************************************************************/
function custom_admin_menu_order( $menu_ord ) {
	if ( !$menu_ord ) return true;

	return array(
		'index.php', // Dashboard
		'separator1', // First separator
		'edit.php?post_type=page', // Pages
		'edit.php?post_type=produkty', // Pages
		'edit.php?post_type=referencia', // Pages
		'edit.php', // Posts
		'upload.php', // Media
		'link-manager.php', // Links
		'separator2', // Second separator
		'themes.php', // Appearance
		'plugins.php', // Plugins
		'users.php', // Users
		'tools.php', // Tools
		'options-general.php', // Settings
		'separator-last' // Last separator
	);
}
add_filter( 'custom_menu_order', 'custom_admin_menu_order', 10, 1 );
add_filter( 'menu_order', 'custom_admin_menu_order', 10, 1 );


/************************************************************************************/
/* Remove from admin menu */
/************************************************************************************/
function menu_remove() { 
	remove_menu_page( 'edit-comments.php' );
}
add_action('admin_menu', 'menu_remove');
/* ========================================================================== 
    Custom ímage sizes
    ========================================================================== */
add_image_size( 'slider', 1140, 700, true);
add_image_size( 'custom-project', 260, 194, true);
add_image_size( 'custom-project-thumb', 260, 190, true);
add_image_size( 'blog-thumbs', 300, 225, true);
add_image_size( 'custom-gallery', 285, 215, true);
add_image_size( 'custom-blog-post', 170, 128, false);
add_image_size( 'hp-img', 805, 455, true);
add_image_size( 'hp-img-slider', 1920, 600, true);
add_image_size( 'portret', 220, 305, false);
add_image_size( 'showroom', 547, 410, true);
add_image_size( 'vyhody', 430, 280, true);
add_image_size( 'onas', 428, 321, false);


/* ========================================================================== 
    Custom pagination
    ========================================================================== */
function custom_pagination($numpages = '', $pagerange = '', $paged='') {

    if (empty($pagerange)) {
        $pagerange = 2;
    }

    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if(!$numpages) {
            $numpages = 1;
        }
    }

    $prev_text = "<";
    $next_text = ">";


    $pagination_args = array(
        'base'            => get_pagenum_link(1) . '%_%',
        'format'          => 'page/%#%',
        'total'           => $numpages,
        'current'         => $paged,
        'show_all'        => true,
        'end_size'        => 1,
        'mid_size'        => $pagerange,
        'prev_next'       => true,
        'prev_text'       => __($prev_text),
        'next_text'       => __($next_text),
        'type'            => 'array',
        'add_args'        => false,
        'add_fragment'    => ''
    );

    $paginate_links = paginate_links($pagination_args);

    if ($paginate_links) {
        echo "<div class='paginator'>";
            if( $paged == 1) {
                echo '<div class="prev paginator__item paginator__item--disabled"><</div>';
            }

            foreach ($paginate_links as $key => $link) {
                if( !is_array($link) ) {
                    $link = str_replace( 'page-numbers', 'paginator__item', $link );
                    $link = str_replace( 'current', 'active', $link );
                    echo $link;
                } 
            }

            if( $paged == $numpages ) {
                echo '<div class="next paginator__item paginator__item--disabled">></div>';
            }
        
        echo "</div>";
    }
}
/* ========================================================================== 
    Custom excerpt function
   ========================================================================== */
function custom_field_excerpt() {
    global $post;
    $text = get_field('obsah_pod_obrazkom');
    if ( '' != $text ) {
        $start = strpos($text, '<p>'); 
        $end = strpos($text, '</p>', $start); 
        $text = substr($text, $start, $end-$start+4);
        $text = strip_shortcodes( $text );
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]>', $text);
    }
    return $text;
}


add_action('acf/render_field_settings/type=image',
           'add_default_value_to_image_field', 20);
function add_default_value_to_image_field($field) {
  $args = array(
    'label' => 'Default obrázok',
    'instructions' => 'Default obrázok pre nový post',
    'type' => 'image',
    'name' => 'default_value'
  );
  acf_render_field_setting($field, $args);
}
add_action('admin_enqueue_scripts', 'enqueue_uploader_for_image_default');
function enqueue_uploader_for_image_default() {
  $screen = get_current_screen();
  if ($screen && $screen->id && ($screen->id == 'acf-field-group')) {
    acf_enqueue_uploader();
  }
}
add_filter('acf/load_value/type=image', 'reset_default_image', 10, 3);
function reset_default_image($value, $post_id, $field) {
  if (!$value) {
    $value = $field['default_value'];
  }
  return $value;
}
