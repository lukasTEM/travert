<?php
/* Template Name: produkty */

get_header(); ?>	
<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap toppadding-sm">

				<div class="container clearfix">

					<div class="col_full">

						<div class="heading-block center nobottomborder">
             
      				<h2><?php echo get_the_title(); ?></h2>
							<span><?php the_field('hlavny_podnadpis'); ?></span>
						</div>
        
						<div class="col_full portfolio-single-image">
						<div class="fslider" data-arrows="true" data-animation="slide">
							<div class="flexslider">
								<div class="slider-wrap">
                <?php 
                  $images = get_field('galeria');
                  if( $images ): ?>
                <?php foreach( $images as $image ): ?>
                <div class="slide"><a href="#"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></a></div>
               <?php endforeach; ?>
              	</div>
              <?php endif; ?>
							</div>
						</div>
					</div><!-- .portfolio-single-image end -->

					</div>

				</div>

				<div class="container clearfix">
          
					<blockquote class="topmargin bottommargin">
						<p><?php the_field( "obsah_pod_galeriou" ); ?></p>
					</blockquote>

					<div class="heading-block center bottommargin-lg product-icon-headline">
						<h3><?php the_field( "nadpis_sekcie_" ); ?></h3>
					</div>
         
         <?php if( have_rows('content_+_ikony') ): ?>

        	
           
        	<?php while( have_rows('content_+_ikony') ): the_row(); 
            
        		// vars
        		$nadpis = get_sub_field('nadpis');
        		$obsah = get_sub_field('obsah');
            $ikona = get_sub_field('ikona');
        	
          ?>
              <div class="col_one_third col_last">
              <div class="feature-box fbox-plain">
							<div class="fbox-icon">
								<i class="<?php echo $ikona; ?>"></i>
							</div>
              <h3><?php echo $nadpis; ?></h3>
							<p><?php echo $obsah; ?></p>
						</div>
          </div>
            <?php endwhile; ?>
			      
        
        	<?php endif; ?>
       
				     <div class="clear"></div>
				

					<div class="divider divider-center"><i class="icon-circle"></i></div>
          </div>
          <div class="container clearfix">
					<!-- Related Portfolio Items
					============================================= -->
					<h4>Realizácie firmy TRAVERT:</h4>
					<div id="related-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="30" data-nav="false" data-autoplay="5000" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-xl="4">
            <?php
          $loop = new WP_Query(array('post_type' => 'realizacie', 'posts_per_page' => 8, 'orderby' => 'ID', 'order' => 'DESC'));
          if ($loop->have_posts()):
            while($loop->have_posts()) {
              $loop->the_post();
              ?>
             <div class="oc-item">
							<div class="iportfolio">
								<div class="portfolio-image">
                    <a href="<?php echo get_permalink(); ?>">
                    <img src="<?php echo get_the_post_thumbnail_url(); ?>">
                    </a>
                    <div class="portfolio-overlay">
    									<a href="<?php echo get_permalink(); ?>" class="center-icon"><i class="icon-book2"></i></a>
    								</div>
    							</div>
    							<div class="portfolio-desc">
    								<h3><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
    								<span><span><?php $categories = get_the_category();
                    $separator = ' ';
                    $output = '';
                    if ( ! empty( $categories ) ) {
                        foreach( $categories as $category ) {
                            $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '  </a>' . $separator;
                        }
                        echo trim( $output, $separator );
                    } ?> </span></span>
    							</div>
    						 </div>
						</div>
               <?php
            }
          endif;
          wp_reset_query();
          ?>
				
			

					</div><!-- .portfolio-carousel end -->
         	</div>
              	
          </div>
			
	<div class="content-wrap">
				
				<!-- CTA
				============================================= -->
				<a href="<?php the_field('odkaz_pre_button'); ?>" class="button button-full center tright footer-stick">
					<div class="container clearfix">
						<?php the_field('cta'); ?> <i class="icon-caret-right" style="top:4px;"></i>
					</div>
				</a>
     </div>
		

		</section><!-- #content end -->
<?php get_footer(); ?>