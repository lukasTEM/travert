<!DOCTYPE html>
<html dir="ltr" lang="sk-SK">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Studio TEM" />

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Icons
	============================================= -->
	<link rel="icon"
	href="<?php echo get_template_directory_uri() . '/uploads/Travert_favicon_16x16.ico' ?>"
	type="image/png" sizes="16x16">
	<link rel="icon"
	href="<?php echo get_template_directory_uri() . '/uploads/Travert_favicon_16x16.ico' ?>"
	type="image/png" sizes="32x32">
	<link rel="apple-touch-icon"
	href="<?php echo get_template_directory_uri() . '/uploads/Travert_apple-touch-icon_57x57.indd' ?>"
	>
	<link rel="apple-touch-icon" sizes="57x57"
	href="<?php echo get_template_directory_uri() . '/uploads/Travert_apple-touch-icon_57x57.indd' ?>"
	>

	<!-- WP Head
	============================================= -->
	<?php wp_head() ?>

	<!-- Link in style
	============================================= -->
	<style type="text/css">
		#page-title.page-title-pattern {
			background-image: url('<?php echo get_template_directory_uri() . "/images/pattern.png" ?>');
			background-repeat: repeat;
			background-attachment: fixed;
		}
	</style>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5W94X5B');</script>
	<!-- End Google Tag Manager -->

</head>

<body <?php body_class( "no-transition" ); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5W94X5B"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="transparent-header semi-transparent static-sticky full-header" data-sticky-class="not-dark">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="<?php echo get_home_url(); ?>" 
						class="standard-logo" 
						data-dark-logo="<?php echo get_template_directory_uri() . '/uploads/logo.png' ?>">
							<img src="<?php echo get_template_directory_uri() . '/uploads/logo.png' ?>" 
							alt="Travert Logo">
						</a>
						<a href="<?php echo get_home_url(); ?>" 
						class="retina-logo" 
						data-dark-logo="<?php echo get_template_directory_uri() . '/uploads/logo.png' ?>">
							<img src="<?php echo get_template_directory_uri() . '/uploads/logo.png' ?>" 
							alt="Travert Logo">
						</a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu" class="dark style-5">

					<?php
          wp_nav_menu( array(
          'theme_location' => 'header-menu',
          'container_class' => 'sf-js-enabled'

           ) );
          ?>

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->
