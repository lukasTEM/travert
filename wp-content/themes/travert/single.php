<?php
/* Template Name: Single blog post */

get_header(); ?>
<section id="content">

			<div class="content-wrap toppadding-xs">

				<div class="container clearfix">

					<div class="single-post nobottommargin">

						<!-- Single Post
						============================================= -->
						<div class="entry clearfix">

							<!-- Entry Title
							============================================= -->
							<div class="entry-title">
								<h2><?php echo get_the_title(); ?></h2>
							</div><!-- .entry-title end -->

							<!-- Entry Meta
							============================================= -->
							<ul class="entry-meta clearfix">
								<li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
								<li><i class="icon-folder-open"></i><?php $categories = get_the_category();
                        if ( ! empty( $categories ) ) {
                        echo esc_html( $categories[0]->name );}?></li>
							</ul><!-- .entry-meta end -->

							<!-- Entry Image
							============================================= -->
             
							<div class="entry-image bottommargin">
              <?php 
              $hlavna_fotka = get_field('hlavna_fotka');
              if( !empty($hlavna_fotka) ): ?>
								<img src="<?php echo $hlavna_fotka['sizes']['slider']; ?>" alt="">
              <?php endif; ?>
							</div><!-- .entry-image end -->

							<!-- Entry Content
							============================================= -->
							<div class="entry-content notopmargin">
              <?php the_field('obsah_pod_obrazkom'); ?>
						

							
                
								<!-- Gallery
								============================================= -->
								<div class="divider"><i class="icon-circle"></i></div>

								<div class="col_full clearfix">

									<h3>Galéria</h3>
                  <div class="masonry-thumbs grid-4" data-lightbox="gallery">
                <?php 
                  $images = get_field('galeria');
                  if( $images ): ?>
                <?php foreach( $images as $image ): ?>
                	<a href="<?php echo $image['url']; ?>" data-lightbox="gallery-item"><img class="image_fade" src="<?php echo $image['sizes']['custom-gallery']; ?>" alt="<?php echo $image['alt']; ?>"></a>
                
               <?php endforeach; ?>
              	</div>
              <?php endif; ?>
							</div>
						</div>
											

								<!-- Tag Cloud
								============================================= -->
								<div class="tagcloud clearfix bottommargin">
                <?php
                $posttags = get_the_tags();
                if ($posttags) {
                  echo '<ul>';
                  foreach($posttags as $tag) {
                    echo '<a>' .$tag->name. '</a>'; 
                  }
                  echo '</ul>';
                }
                ?> 
                </div><!-- .tagcloud end -->

								<div class="clear"></div>

								<!-- Post Single - Share
								============================================= -->
              
								 <?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
               
               
							</div>
						</div><!-- .entry end -->

						<!-- Post Navigation
						============================================= -->
				
						<div class="post-navigation clearfix">

							<div class="col_half nobottommargin">
                <?php previous_post_link('%link', '&lArr; Predchádzajúci článok')?>
								</div>

							<div class="col_half col_last tright nobottommargin">
              <?php next_post_link('%link', 'Nasledujúci článok &rArr;')?>
							</div>

						</div> <!-- .post-navigation end   -->

					 <div class="line"></div>

						<!-- Other Posts
						============================================= -->
						<h4>Ďalšie články:</h4>
             <?php
          $loop = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 4, 'orderby' => 'rand', 'order' => 'DESC',
				'date_query' => array(
					'after' => date('Y-m-d', strtotime('-720 days')) 
				)
			));
          if ($loop->have_posts()):
            while($loop->have_posts()) {
              $loop->the_post();
              ?>
            

							<div class="col_half nobottommargin col_last">

								<div class="mpost clearfix">
									<div class="entry-image">
                  <?php 
                  $hlavna_fotka = get_field('hlavna_fotka');
                  if( !empty($hlavna_fotka) ): ?>
                  <a href="<?php echo get_permalink(); ?>"><img src="<?php echo $hlavna_fotka['sizes']['custom-blog-post']; ?>" alt="Blog Single"></a>  <?php endif; ?>
                 	</div>
                    <div class="entry-c">
                		<div class="entry-title">
                			<h4><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></h4>
                		</div>
                		<ul class="entry-meta clearfix">
                			<li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
                			<li><i class="icon-folder-open"></i><?php $categories = get_the_category();
                        if ( ! empty( $categories ) ) {
                        echo esc_html( $categories[0]->name ) ;}?></li>
                		</ul>
                		<div class="entry-content">
                			<p><?php the_excerpt(); ?></p>
                			</div>
                	   </div>
                  </div>
               </div>
            
                   
               <?php
            }
          endif;
          wp_reset_query();
          ?>
           
					

					</div>

				</div>

			</div>

		</section><!-- #content end -->

<?php get_footer(); ?>